package com.buubuu.resources;

import com.buubuu.api.Player;
import com.buubuu.api.PlayerRequest;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Path("/leaderboard")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {

    private static final int MAX_PLAYERS = 10;
    private static final String TABLE = "leaderboard";

    public HelloWorldResource() {
    }

    @GET
    @Timed
    public List<PlayerRequest> getScores() throws URISyntaxException, SQLException {
        List<PlayerRequest> data = new ArrayList<PlayerRequest>();

        for (Player player : selectTopPlayers()) {
            data.add(new PlayerRequest(player.getName(), player.getScore()));
        }

        return data;
    }

    @POST
    @Timed
    public Player addScore(
            @HeaderParam("x-buubuu-auth") String authString,
            PlayerRequest playerRequest
    ) throws URISyntaxException, SQLException {
        Player player = new Player(playerRequest.getName(), playerRequest.getScore());

        if (authString == null) {
            player.setNote("unauthorized");

            return player;
        }

        byte[] decode = Base64.getDecoder().decode(authString);
        String value = new String(decode).replace(System.getenv("BUUBUU_AUTH"), "");
        String playerValue = player.getName() + String.valueOf(player.getScore());
        System.out.println("value=" + value);
        System.out.println("playerValue=" + playerValue);

        if (!value.equals(playerValue)) {
            player.setNote("unauthorized");

            return player;
        }

        List<Player> players = selectTopPlayers();

        if (players.size() < MAX_PLAYERS) {
            if (insert(player)) {
                player.setNote("ok");
            } else {
                player.setNote("fail");
            }

            return player;
        }

        Player lastPlayer = players.get(players.size() - 1);

        if (player.getScore() <= lastPlayer.getScore()) {
            player.setNote("skip");

            return player;
        }

        if (delete(lastPlayer) && insert(player)) {
            player.setNote("ok");
        } else {
            player.setNote("fail");
        }

        return player;
    }

    private List<Player> selectTopPlayers() throws URISyntaxException, SQLException {
        List<Player> players = new ArrayList<Player>();

        try (
                Connection conn = getConnection();
                Statement statement = conn.createStatement();
        ) {
            createTableIfNotExist(statement);

            String strSelect = "SELECT * FROM " + TABLE + " ORDER BY score DESC";
            System.out.println("The SQL query is: " + strSelect);
            System.out.println();

            ResultSet rset = statement.executeQuery(strSelect);

            System.out.println("The records selected are:");
            int rowCount = 0;
            while (rset.next()) {
                String id = rset.getString("id");
                String name = rset.getString("name");
                int score = rset.getInt("score");

                players.add(new Player(id, name, score));

                System.out.println(id + ", " + name + ", " + score);
                ++rowCount;
            }
            System.out.println("Total number of records = " + rowCount);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return players;
    }

    private boolean insert(Player player) throws URISyntaxException, SQLException {
        try (
                Connection conn = getConnection();
                Statement statement = conn.createStatement();
        ) {
            createTableIfNotExist(statement);

            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO " + TABLE + " (id, name, score) VALUES (?, ?, ?)");
            preparedStatement.setString(1, player.getId());
            preparedStatement.setString(2, player.getName());
            preparedStatement.setInt(3, player.getScore());

            System.out.println("The SQL query is: " + preparedStatement.toString());

            int countInserted = preparedStatement.executeUpdate();
            System.out.println(countInserted + " records inserted.\n");

            return countInserted > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();

            return false;
        }
    }

    private boolean delete(Player player) throws URISyntaxException, SQLException {
        try (
                Connection conn = getConnection();
                Statement statement = conn.createStatement();
        ) {
            createTableIfNotExist(statement);

            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM " + TABLE + " WHERE id = ?");
            preparedStatement.setString(1, player.getId());

            System.out.println("The SQL query is: " + preparedStatement.toString());

            int countDeleted = preparedStatement.executeUpdate();
            System.out.println(countDeleted + " records deleted.\n");

            return countDeleted > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();

            return false;
        }
    }

    private static Connection getConnection() throws URISyntaxException, SQLException {
        URI jdbUri = new URI(System.getenv("JAWSDB_URL"));

        String username = jdbUri.getUserInfo().split(":")[0];
        String password = jdbUri.getUserInfo().split(":")[1];
        String port = String.valueOf(jdbUri.getPort());
        String jdbUrl = "jdbc:mysql://" + jdbUri.getHost() + ":" + port + jdbUri.getPath();

        return DriverManager.getConnection(jdbUrl, username, password);
    }

    private void createTableIfNotExist(Statement statement) throws SQLException {
        String sqlCreate = "CREATE TABLE IF NOT EXISTS " + TABLE + " (id varchar(32) NOT NULL, name varchar(64) NOT NULL, score int, PRIMARY KEY(id))";
        statement.execute(sqlCreate);
    }

}
