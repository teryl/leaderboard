package com.buubuu.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import java.util.UUID;

public class Player {

    @Length(max = 32)
    private String id;

    @Length(max = 64)
    private String name;

    private int score;

    private String note;

    public Player() {
    }

    public Player(String name, int score) {
        this(UUID.randomUUID().toString().replace("-", ""), name, score);
    }

    public Player(String id, String name, int score) {
        this.id = id;
        this.name = name;
        this.score = score;
    }

    @JsonProperty
    public String getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public int getScore() {
        return score;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

}
