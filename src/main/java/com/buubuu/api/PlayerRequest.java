package com.buubuu.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

public class PlayerRequest {

    @Length(max = 64)
    private String name;

    private int score;

    public PlayerRequest() {
    }

    public PlayerRequest(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public int getScore() {
        return score;
    }

}
